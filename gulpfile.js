///* REQUIRED *///

var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	cssnano = require('gulp-cssnano'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	sass = require('gulp-sass'),
	imagemin = require('gulp-imagemin');

///* TASKS *///

/*Minify JS files*/

gulp.task('concat', function() {
	return gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/popper.js/dist/umd/popper.min.js',
		'node_modules/bootstrap/js/dist/button.js',
		'node_modules/bootstrap/js/dist/dropdown.js',
		'node_modules/bootstrap/js/dist/util.js',
		'node_modules/bootstrap/js/dist/collapse.js',
		'src/js/index.js',
		'src/js/lightbox.js'
	])
		.pipe(concat('bundle.js'))
		.pipe(gulp.dest('src/js/bundle'));
});

gulp.task('uglify', ['concat'], function() {
	return gulp.src('src/js/bundle/bundle.js')
		.pipe(uglify())
		.pipe(gulp.dest('dist/js'));
});


/*Sass tasks, sourcemaps and minify*/
gulp.task('sass', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sourcemaps.init())
    	.pipe(sass().on('error', sass.logError))
        .pipe(cssnano())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/css'));
});


/*Minify images*/

gulp.task('imagemin', function () {
    return gulp.src('src/img/*')
		.pipe(imagemin([
		    imagemin.gifsicle({interlaced: true}),
		    imagemin.jpegtran({progressive: true}),
		    imagemin.optipng({optimizationLevel: 5}),
		    imagemin.svgo({
		        plugins: [
		            {removeViewBox: true},
		            {cleanupIDs: false}
		        ]
		    })
		]))
		.pipe(gulp.dest('dist/img'));
});


///* WATCH TASK *///
gulp.task('watch', function(){
	gulp.watch('src/js/*.js', ['uglify']);
	gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss', 'src/scss/*.css'], ['sass']);
});

///* DEFAULT TASK *///
gulp.task('default', ['concat', 'uglify', 'sass', 'watch']);